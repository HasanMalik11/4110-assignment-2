import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EquivalenceTest0 {

	@Test
	void test() {
		trig trig = new trig();
		
		int output = trig.Sin(0);
		assertEquals(Math.sin(0), output);

		output = trig.Cos(0);
		assertEquals(Math.cos(0), output);
		
		output = trig.Tan(0);
		assertEquals(Math.tan(0), output);
	}

}
