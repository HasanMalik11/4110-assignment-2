//Boundary Test 
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class test {

	@Test
	void test() {
	
		trig t = new trig();
		int output=t.Sin(2147483647);
		assertEquals(Math.sin(2147483647), output);

		output=t.Sin(-2147483648);
		assertEquals(Math.sin(-2147483648), output);
		
		output=t.Cos(2147483647);
		assertEquals(Math.cos(2147483647), output);
		
		output=t.Cos(-2147483648);
		assertEquals(Math.cos(-2147483648), output);
		
		output=t.Tan(2147483647);
		assertEquals(Math.tan(-2147483648), output);
		
		output=t.Tan(-2147483648);
		assertEquals(Math.tan(-2147483648), output);
	}

}
