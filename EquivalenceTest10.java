import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EquivalenceTest10 {

	@Test
	void test() {
		trig trig = new trig();
		
		int output = trig.Sin(10);
		assertEquals(Math.sin(10), output);

		output = trig.Cos(10);
		assertEquals(Math.cos(10), output);
		
		output = trig.Tan(10);
		assertEquals(Math.tan(10), output);
	}

}
