import java.util.Scanner;

public class trig{

	int answer = 0;
    boolean step = false;

	public int Sin(final int x) {

		for (int i = 1; i < 100; i += 2) {

			if (step == false) {
				answer += Math.pow( x, i) / fact(i);
				step = true;
			}

			if (step == true) {
				answer -= (Math.pow( x, i)) / fact(i);
				step = false;
			}
		}

		return answer;
	}

	public int Cos(final int x) {
		answer = 1;

		for (int i = 2; i < 100; i += 2) {

			if (step == false) {
				answer += Math.pow(x, i) / fact(i);
				step = true;
			}

			if (step == true) {
				answer -= (Math.pow(x, i)) / fact(i);
				step = false;
			}

		}

		return answer;
	}

	public int Tan(final int x) {

		answer = Sin(x) / Cos(x);
		return answer;

	}

	int fact(final int x) {
		 int i, factorial = 1;  
		 
		 if(x == 0) {
			 return 1;
		 }
		      
		  for(i = 1; i <= x; i++){    
		      factorial = factorial * i;    
		  }
		  
		  return factorial;
	}
}


